# Contributor: Kevin Daudt <kdaudt@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=chezmoi
pkgver=2.9.0
pkgrel=0
pkgdesc="Manage your dotfiles across multiple machines, securely."
url="https://www.chezmoi.io/"
arch="all"
license="MIT"
makedepends="go"
options="!check chmod-clean" # no test suite
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/twpayne/chezmoi/archive/v$pkgver.tar.gz"

export GOPATH="$srcdir"

build() {
	go build \
		-ldflags "
		-X \"main.version=$pkgver\" \
		-X \"main.date=$(date --utc +%Y-%m-%dT%H:%M:%SZ)\" \
		-X \"github.com/twpayne/chezmoi/cmd.DocsDir=/usr/share/doc/chezmoi/\"
		" \
		-tags noupgrade \
		-tags noembeddocs
	make completions
}

package() {
	install -Dm0755 chezmoi "$pkgdir"/usr/bin/chezmoi
	install -Dm0644 completions/chezmoi-completion.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm0644 completions/chezmoi.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname
	install -Dm0644 completions/chezmoi.fish \
		"$pkgdir"/usr/share/fish/completions/$pkgname.fish
}

doc() {
	default_doc
	mkdir -p "$subpkgdir/usr/share/doc/chezmoi"
	cp "$builddir/docs/"* "$subpkgdir/usr/share/doc/chezmoi"
}

sha512sums="
f9e4457043966045042977c489715439ed7b1d7659afa4f7117575c0b0fb63d49389208f21808af433f2fc85a52cfbd783e2b2e9bcdc7e91d4ff9e552b622b42  chezmoi-2.9.0.tar.gz
"
